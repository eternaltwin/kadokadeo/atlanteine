import pixi.core.Application;
import js.Browser;

class Manager {
	public static var app:Application;

	static var game:Game;
	static var baseSprite:ASprite;

	static public function preload() {
		return BmpTextureHelper.preload(['mcDalle', 'mcWall', 'mcOut']);
	}

	static public function main() {
		KeyboardManager.init();

		if (Manager.app != null)
			return;

		var view = cast js.Browser.document.getElementById("main");
		app = new Application({width: 300, height: 300, view: view});

		preload().then((_) -> {
			baseSprite = new ASprite();
			app.stage.addChild(baseSprite);
			game = new Game(baseSprite);
			Game.me = game;

			Browser.window.requestAnimationFrame(update);
		});
	}

	static function update(ts:Float) {
		mt.Timer.update(ts);

		baseSprite.update();
		game.update(ts);

		Browser.window.requestAnimationFrame(update);
	}
}
