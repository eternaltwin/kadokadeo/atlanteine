import haxe.ds.StringMap;

class SpriteData {
	static public var data:StringMap<Array<Int>> = [
		"mcBall" => [39, 71],
		"mcWall" => [33, 35],
		"mcBg" => [301, 301],
		"mcBonus" => [31, 25],
		"mcDalle" => [35, 46],
		"mcOut" => [32, 32],
		"mcDebugField" => [32, 31],
		"mcTeleport" => [30, 30],
		"mcDisplacement" => [301, 301],
		"mcGhost" => [31, 31],
		"mcLine" => [151, 51],
		"mcPoint" => [51, 51],
		"mcPush" => [28, 18],
		"mcRay" => [101, 41],
		"mcScore" => [47, 24],
		"mcSquare" => [38, 52],
		"mcStartDalle" => [18, 18],
		"mcTimer" => [51, 51],
		"partBlock" => [19, 17],
		"partCloud" => [16, 16],
		"partLight" => [21, 21],
		"partTiret" => [7, 2],
		"partWarp" => [292, 239],
		"quarterAnim" => [37, 26],
	];
	static public var anchor:StringMap<Array<Int>> = new StringMap();
}
